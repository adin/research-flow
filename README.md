# About

This project holds the [wiki](/../wikis/home) to explain conventions and definitions to do research based on git. *Gitflow research*, if you like.

# Wiki

To obtain the wiki

```bash
git clone git@gitlab.com:adin/research-flow.wiki.git
cd research-flow.wiki
```

## Dependencies

```bash
gem install gollum
```

## More details

Check more information in the [git access page](/../wikis/git_access).

Or directly check the [wiki](/../wikis/home).
