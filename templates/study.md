# Topic/Problem

Explain the problem in detail.

# Motivation

Explain the motivation behind the problem.
How the problem is related to other parent problems or how it stems from others.

# Main Ideas

Detail the main ideas studied.

## Tools

Explain algorithms, tools, and frameworks developed or used to solve/tackle the current problem.

# References

List the references used for this report.

