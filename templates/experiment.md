# Problem

Explain the problem in detail that you are trying to solve with the current experiment.

# Setup
## Data

Describe the data (datasets, partitions, or other data related setup) used for the current experiments.

## Methods

Explain the algorithms that solves the above problem. Specially, describe the parameters used to perform the current experiment.

## Experiment

Describe the setup of the experiment. In case you are using an existing setup, reference it and briefly describe it.

# Results and Discussion

Detail the results and explain the findings. Note that a discussion is not state what can be seen in the graphs or figures shown. The discussion enhances and improves the results shown.

# Conclusion

Conclude your findings.

# References

List the references used for this report.
