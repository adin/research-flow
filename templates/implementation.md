# Problem

Explain the problem to solve in detail.

# Algorithm

Explain the algorithm that solves the above problem.

## Tools

Briefly explain algorithms, tools, and frameworks developed or used to solve the current problem.

